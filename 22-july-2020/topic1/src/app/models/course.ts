export interface Course {
  title: String;
  description: String;
}
