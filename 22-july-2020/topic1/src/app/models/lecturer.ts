export interface Lecturer {
  id: number;
  name: String;
}
