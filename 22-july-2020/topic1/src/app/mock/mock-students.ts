import {Student} from '../models/student';
export const STUDENTS: Student[] = [
  {id: 61114440144, name: 'chaiwat', email: 'chaiwat@gmail.com'},
  {id: 61114441112, name: 'singkibut', email: 'singkibut@gmail.com'},
  {id: 61114443333, name: 'fariw', email: 'fariw@gmail.com'},
  {id: 61114444444, name: 'leo', email: 'leo@gmail.com'},
  {id: 61114445555, name: 'Lee', email: 'Lee@gmail.com'},
];
