import { Component } from '@angular/core';
import { Student } from './models/student';
import { STUDENTS } from './mock/mock-students';

import { Course } from './models/course';
import { COURSE } from './mock/mock-course';

import { Score } from './models/score';
import { SCORE } from './mock/mock-score';

import { Lecturer } from './models/lecturer';
import { LECTURER } from './mock/mock-lecturer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Web Application';
  discription = "คำอธิบายรายวิชา";

  students: Student[] = STUDENTS;
  course: Course[] = COURSE;
  lecturer: Lecturer[] = LECTURER;
  score: Score[] = SCORE;
}
