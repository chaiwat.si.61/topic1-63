import * as readline from 'readline'

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.question('กรอกชื่อ: ', (answer) => {
  console.log(`สวัสดีคุณ ${answer}`)
  rl.close()
})
