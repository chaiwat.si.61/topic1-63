import { Component } from '@angular/core';

interface Lecturer {
  name: string;
  image: string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hello Larm';

  //baseurl = 'http://picsum.photos/id/';

  lecturers: Lecturer[] = [
    { image: 'http://picsum.photos/id/1001/84/84', name: 'Dr.K' },
    { image: 'http://picsum.photos/id/1002/84/84', name: 'Assist.Prof.Chayaporn' },
    { image: 'http://picsum.photos/id/1003/84/84', name: 'Dr.Tossaporn' },
    { image: 'http://picsum.photos/id/1004/84/84', name: 'Dr.Phaichayon' },
    { image: 'http://picsum.photos/id/1005/84/84', name: 'Dr.Woody' },
    { image: 'http://picsum.photos/id/1006/84/84', name: 'Aj.Wasana' },
    { image: 'http://picsum.photos/id/1037/84/84', name: 'Dr.Paul' },
    { image: 'http://picsum.photos/id/1008/84/84', name: 'Dr.Suphawadee' },
    { image: 'http://picsum.photos/id/1009/84/84', name: 'Aj.Wayo' }
  ];
}
