import { Component, OnInit } from '@angular/core';

interface Lecturer {
  name:  string;
  image: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  baseurl = 'http://';

  lecturers: Lecturer[] = [
      { image: 'http://picsum.photos/id/1001/84/84', name: 'Dr.K' },
      { image: 'http://picsum.photos/id/1001/84/84', name: 'Assist.Prof.Chayaporn' },
      { image: 'http://picsum.photos/id/1001/84/84', name: 'Dr.Tossaporn' },
      { image: 'http://picsum.photos/id/1001/84/84', name: 'Dr.Phaichayon' },
      { image: 'http://picsum.photos/id/1001/84/84', name: 'Dr.Woody' },
      { image: 'http://picsum.photos/id/1001/84/84', name: 'Aj.Wasana' },
      { image: 'http://picsum.photos/id/1001/84/84', name: 'Dr.Paul' },
      { image: 'http://picsum.photos/id/1001/84/84', name: 'Dr.Suphawadee' },
      { image: 'http://picsum.photos/id/1001/84/84', name: 'Aj.Wayo' }
   ];

  constructor() { }

  ngOnInit() {
  }

}

