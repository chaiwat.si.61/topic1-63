import { app, BrowserWindow } from 'electron';

let win = null
app.on('ready', () => {
    win = new BrowserWindow({
        width: 500,
        height: 400
    })
    win.setTitle('หน้าต่าง')
    win.on('closed', () => { win = null })
});