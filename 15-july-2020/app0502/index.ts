import { app, BrowserWindow } from 'electron'

let win = null;

function createWindow() {
  win = new BrowserWindow({width: 500, height: 400})
  win.setTitle('CSUBU Lecturers')
  win.loadFile('index.html')
  win.on('closed', () => { win = null })
}

app.on('ready', () => {
    createWindow();
})
