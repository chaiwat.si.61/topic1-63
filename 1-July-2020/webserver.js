const http = require('http');
 
const hostname = '127.0.0.1';
const port = 3000;
 
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('etracking-api-key', 'b3394f93a2af31932016586f58bc3894654351f7');
  res.setHeader('etracking-key-secret', 'e7e5c54317cb01493db04d321b25fa4027973cb09a4a8145342f1d8d7985d03af63f9656f27fc8ba345dfb4a69acc530ee0c637e1179e16f4e4db1cf45f5b296bc12924db3e52c47bcc2dc');
  res.setHeader('Content-Type', 'application/json');


  res.end('hello world');
});
 
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});





// var http = require('http');
// http.createServer(function (req, res) {
//   res.writeHead(200, {'Content-Type': 'text/plain'});
//   res.end('Hello World\n');
// }).listen(1337, '127.0.0.1');
// console.log('Server running at http://127.0.0.1:1337/');